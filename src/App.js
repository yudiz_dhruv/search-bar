import React from 'react'
import './App.css'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import List from './components/List'

function App () {
  return (
    <div>
      <List />
    </div>
  )
}

export default App
